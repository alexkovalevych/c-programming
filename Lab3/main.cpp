//
//  main.c
//  Lab3
//
//  Created by Alex Kovalevych on 3/12/13.
//
//

#include <stdio.h>

typedef struct complex {
    float r;
    float i;
} complex;

complex createComplex(float r)
{
    complex number;
    number.r = r;
    number.i = 0;
    
    return number;
}

complex createComplex(float r, float i)
{
    complex number;
    number.r = r;
    number.i = i;
    
    return number;
}

complex sum(complex number1, complex number2)
{
    complex result;
    result.r = number1.r + number2.r;
    result.i = number1.i + number2.i;
    
    return result;
}

complex multiplication(complex number1, complex number2)
{
    complex result;
    result.r = number1.r * number2.r;
    result.i = number1.i * number2.i;
    
    return result;
}

complex inverse(complex number)
{
    number.r = -number.r;
    number.i = -number.i;
    
    return number;
}

complex minus(complex number1, complex number2)
{
    complex result;
    result.r = number1.r - number2.r;
    result.i = number1.i - number2.i;
    
    return result;
}

complex division(complex number1, complex number2)
{
    complex result;
    result.r = number1.r / number2.r;
    result.i = number1.i / number2.i;
    
    return result;
}

int main(int argc, const char * argv[])
{
    float u1, u2, v1, v2, w1, w2;
    complex u, v, w, y;
    
    printf("Enter numbers u1:");
    scanf("%f", &u1);
    printf("Enter numbers u2:");
    scanf("%f", &u2);
    u.r = u1;
    u.i = u2;

    printf("Enter numbers v1:");
    scanf("%f", &v1);            
    printf("Enter numbers v2:");
    scanf("%f", &v2);
    v.r = v1;
    v.i = v2;
    
    printf("Enter numbers w1:");
    scanf("%f", &w1);
    printf("Enter numbers w2:");
    scanf("%f", &w2);
    w.r = w1;
    w.i = w2;
    
    complex y1 = multiplication(createComplex(2), u);
    complex y2 = division(multiplication(multiplication(createComplex(3), u), w), minus(sum(createComplex(2), w), u));
    complex y3 = inverse(multiplication(createComplex(7), v));
    y = sum(sum(y1, y2), y3);
    if (y.i == 0) {
        printf("Result: %0.2f", y.r);
    } else if (y.i > 0) {
        printf("Result: %0.2f+%0.2fi", y.r, y.i);
    } else {
        printf("Result: %0.2f%0.2fi", y.r, y.i);
    }

    return 0;
}