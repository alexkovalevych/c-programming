//
//  main.c
//  Lab1
//
//  Created by Alex Kovalevych on 3/12/13.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int n, char* a[])
{
    float x, y, z;
    switch (n) {
        case 1:
            printf("Enter x and z values:");
            scanf("%f %f", &x, &z);
            break;
        case 2:
            x = atof(a[1]);
            printf("Please Enter z:");
            scanf("%f", &z);
            break;
        case 3:
        default:
            x = atof(a[1]);
            z = atof(a[2]);
            break;
    }
    
    if (z > x) {
        printf("x should not be less then z");
        return 0;
    }
    
    if (x <= -5) {
        printf("x should be greated than -5");
        return 0;
    }
    
    y = sqrt(pow(x, 3) - pow(z, 3)) + log10(x + 5);
    printf("Result: %f", y);
    
    return 0;
}

