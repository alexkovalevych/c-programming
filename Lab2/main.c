//
//  main.c
//  Lab2
//
//  Created by Alex Kovalevych on 3/12/13.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, const char * argv[])
{
    int n, m, k;
    switch (argc) {
        case 1:
            printf("Enter matrix size:");
            scanf("%i %i", &n, &m);
            break;
        case 3:
        default:
            n = atof(argv[1]);
            m = atof(argv[2]);
            break;
    }

    printf("Enter row to get positive numbers from:");
    scanf("%i", &k);
    
    if (k < 1 || k > n) {
        printf("Row with number %i doesn't exist", k);
        return 0;
    }
    
    float row[m];
    printf("Enter %i matrix row\n", k);
    for (int i = 0; i < m; ++i) {
        printf("Enter element #%i:", i + 1);
        scanf("%f", &row[i]);
    }
    
    for (int i = 0; i < m; ++i) {
        if (row[i] < 0) {
            printf("%0.2f\n", row[i]);
        }
    }
    
    return 0;
}

