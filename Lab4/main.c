//
//  main.c
//  Lab4
//
//  Created by Alex Kovalevych on 3/13/13.
//
//

#include <stdio.h>
#include <math.h>

typedef struct item {
    int row;
    int column;
    int value;
} item;

item createItem(int row, int column, int value)
{
    item element;
    element.row = row;
    element.column = column;
    
    return element;
}

_Bool isPalindrome(int number)
{
    if (number < 10 && number > -10) {
        return 1;
    }
    
    int n = number;
    float reverse;
    while (n != 0) {
        int digit = n % 10;
        reverse = reverse * 10 + digit;
        n = n / 10;
    }
    
    if (reverse == number) {
        return 1;
    }
    
    return 0;
}

int main(int argc, const char * argv[])
{
    int n, size = 0;
    printf("Enter matrix size:");
    scanf("%u", &n);
    for (int i = 0; i < ceil((double) n / 2); ++i) {
        size += n - 2 * i;
    }
    
    item matrix[size];
    int pointer = 0;

    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            if (i + j <= n + 1 && i <= j) {
                item * element = &matrix[pointer];
                element->row = i;
                element->column = j;
                printf("Enter element #%i%i:", i, j);
                scanf("%d", &element->value);
                ++pointer;
            }
        }
    }
    
    for (int i = 0; i < size; ++i) {
        if (isPalindrome(matrix[i].value) && matrix[i].value < 100) {
            printf("%i\n", matrix[i].value);
        }
    }
}